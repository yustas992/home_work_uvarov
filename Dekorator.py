import functools
import os

import requests
import psutil as psutil


def get_process_memory():
    process = psutil.Process(os.getpid())
    mem_info = process.memory_info()
    return mem_info.rss


def profile(msg='Memory'):
    def internal(f):
        @functools.wraps(f)
        def deco(*args, **kwargs):
            memory_start = get_process_memory()
            result = f(*args, **kwargs)
            finish_memory = get_process_memory()
            print(msg, f'({f.__name__}): {finish_memory - memory_start} byte')
            return result

        return deco

    return internal


@profile(msg='Memory occupied for function: ')
def fetch_url(url, first_n=100):
    """Fetch a given url"""
    res = requests.get(url)
    return res.content[:first_n] if first_n else res.content


print(fetch_url('https://reddit.com'))
print(fetch_url('https://google.com'))
