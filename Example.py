import datetime
import json
import random
import string

import requests
import faker
import flask

from flask import request, jsonify, Response

from flask import Flask
from marshmallow import validate
from webargs import fields
from webargs.flaskparser import use_kwargs

from db_utils import execute_query
from utils import format_records


app = Flask(__name__)


@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


@app.route("/")
def hello_world():
    return "Hello, World!"


@app.route("/now")
def get_current_time():
    return str(datetime.datetime.now())


@app.route("/password")
@use_kwargs(
    {
        "length": fields.Int(
            # required=True,
            missing=10,
            validate=[validate.Range(min=1, max=999)],
        )
    },
    location="query",
)
def generate_password(length):
    return ''.join(
        random.choices(
            string.ascii_lowercase + string.ascii_uppercase,
            k=length,
        )
    )


@app.route('/who-is-on-duty')
def get_astronauts():
    url = 'http://api.open-notify.org/astros.json'
    res = requests.get(url)
    if res.status_code not in (200, 204):
        return Response('ERROR: Something went wrong', status=res.status_code)
    result = res.json()
    stats = {}
    for entry in result['people']:
        stats[entry['craft']] = stats.get(entry['craft'], 0) + 1
    return stats


@app.route('/customers')
def get_customers():
    records = execute_query('select * from customers')
    result = format_records(records)
    return result


app.run(port=5001, debug=True)
