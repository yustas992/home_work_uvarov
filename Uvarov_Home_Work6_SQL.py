

from flask import  jsonify

from flask import Flask

from webargs import fields
from webargs.flaskparser import use_kwargs

from db_utils import execute_query
from utils import format_records

app = Flask(__name__)


@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


@app.route("/")
def hello_world():
    return "Hello, World!"


# 1. Вывести общую длительность треков в таблице в секундах, сгруппированную по музыкальным жанрам
@app.route('/genres_durations')
def get_genres_durations():
    query = 'SELECT g.Name , sum(t.Milliseconds)/1000 as Seconds_Duration,COUNT(*) ' \
            'from genres g inner join tracks t on g.GenreId = t.GenreId GROUP by g.Name ' \
            'order by g.Name'
    records = execute_query(query)
    result = format_records(records)
    return result


# Вывести count самых продаваемых треков. count - необязательный параметр, ограничивает кол-во выводимых
# записей. Если не указан, то вернуть все. Показывать поля: названия композиций, сумма и кол-во проданных копий
@app.route('/greatest_hits')
@use_kwargs(
    {
        "Limit": fields.Int(
            required=False,
            missing=None
        )
    }, location="query",
)
def get_greatest_hits(Limit):
    query = "SELECT t.Name as Track_Name,COUNT(*) as Number_of_copies_sold ,sum(ii.UnitPrice) as Price " \
            "from tracks t inner join invoice_items ii on t.TrackId = ii.TrackId group by ii.InvoiceId " \
            "order by Price DESC "
    if Limit:
        query += f'Limit ?'
    if Limit:
        records = execute_query(query, (f'{Limit}',))
    else:
        records = execute_query(query)

    result = format_records(records)
    return result


app.run(port=5001, debug=True)



