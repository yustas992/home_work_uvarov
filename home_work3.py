import random
import string

import requests

from flask import jsonify

from flask import Flask, Response
from marshmallow import validate
from webargs import fields
from webargs.flaskparser import use_kwargs

app = Flask(__name__)


@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


@app.route("/password")
@use_kwargs(
    {
        "length": fields.Int(
            # required=True,
            missing=10,
            validate=[validate.Range(min=1, max=999)]
        ),

        "digits": fields.Int(
            missing=0,
            validate=[validate.Range(min=0, max=1)]
        ),

        "specials": fields.Int(
            missing=0,
            validate=[validate.Range(min=0, max=1)]
        )
    }, location='query'

)
def generate_password(length, digits, specials):
    new_password = string.ascii_lowercase + string.ascii_uppercase
    if digits == 1:
        new_password += string.digits
    if specials == 1:
        new_password += string.punctuation
    general_password = ''.join(
        random.choices(
            new_password,
            k=length
        )
    )
    return general_password


# . Создать view-функцию, которая выводит курс биткойна для заданной валюты (https://bitpay.com/api/rates ).
# Для этого установить и использовать пакет requests: https://pypi.org/project/requests/.
# Параметр currency необязательный, по умолчанию используется USD.


@app.route("/bitcoin_currency")
@use_kwargs(
    {
        'currency': fields.Str(
         missing='USD',
         validate=lambda curr_code: len(curr_code) == 3)
    },
    location='query'
)
def get_bitcoin_currency(currency):
    url = 'https://bitpay.com/api/rates'
    try:
        res = requests.get(url)
        if res.status_code not in (200, 204):
            return Response('ERROR: Something went wrong', status=res.status_code)
        result = res.json()
        for elem in result:
            if elem["code"] == currency:
                res = f" Bitcoin rate {elem['rate']} towards {elem['name']} ({elem['code']})"
                return res

    except Exception as ERROR:
        return f'{ERROR}: Something went wrong{Response.status_code}'


app.run(debug=True)