# 3. Создать в классе Circle (x, y, radius) булевый метод contains, который принимает в качестве параметра точку
# (экземпляр класса Point (x, y)) и проверяет находится ли данная точка внутри окружности.
# Координаты центра окружности и точки могут быть произвольными. Если точка попадает на окружность,
# то это считается вхождением.

class Point:
    def __init__(self, x1, y1):
        if isinstance(x1, (int, float)) and isinstance(y1, (int, float)):
            self.x_coord = x1
            self.y_coord = y1
        else:
            print("Put correct coordinates")
            raise Exception


class Circle:
    def __init__(self, x_centr, y_centr, radius):
        if isinstance(x_centr, (int, float)) and isinstance(y_centr, (int, float)) and isinstance(radius, (int, float)):
            self.x_centr = x_centr
            self.y_centr = y_centr
            self.radius = radius
        else:
            print("Put correct coordinates")
            raise Exception

    def contains(self, p1: Point):
        if isinstance(p1, Point):
            return (((p1.x_coord - self.x_centr) ** 2) + ((p1.y_coord - self.y_centr) ** 2)) <= self.radius**2


p1 = Point(3, 0)
cir = Circle(0, 0, 3)
print(cir.contains(p1))