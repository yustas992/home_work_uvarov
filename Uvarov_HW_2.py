import requests
import flask
import faker
import csv

from flask import Flask
from faker import Faker

app = Flask(__name__)


@app.route("/pipfile")
def get_pipfile():
    with open("Pipfile.lock", 'r') as file:
        return file.read()


@app.route("/random_students")
def get_random_students():
    name_list = []
    for _ in range(11):
        name_list.append(Faker('UK').name())
    return ' '.join(name_list)


@app.route("/avr_data")
def get_avr_data():
    with open('hw.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=",")
        general_height = 0
        general_weight = 0
        for row in reader:
            general_height += float(row[' "Height(Inches)"'])
            general_weight += float(row[' "Weight(Pounds)"'])
        average_height = round((general_height / int(row["Index"])) * 2.54)
        average_weight = round((general_weight / int(row["Index"])) * 0.453592)

        return f'Average height - {average_height} cm , average weight - {average_weight} kg'


app.run(debug=True)
