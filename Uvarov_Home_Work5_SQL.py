from faker import Faker
from flask import jsonify
from flask import Flask
from db_utils import execute_query
from utils import format_records

fake = Faker()

app = Flask(__name__)


@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


@app.route("/")
def hello_world():
    return "Hello, World!"


# 1. Создать view-функцию, которая возвращает количество уникальных имен (FirstName) в таблице Customers.
@app.route('/unique_names')
def get_unique_names():
    records = execute_query('SELECT COUNT(DISTINCT FirstName) FROM customers ')
    return str(records[0][0])


# 2. Создать view-функцию, которая выводит количество записей из таблицы Tracks.
@app.route('/tracks_count')
def get_tracks_count():
    records = execute_query('select count(*) FROM tracks ')
    result = format_records(records)
    return result


# Написать вью-функцию, которая заполняет название компании (поле Company в таблице Customers) для тех клиентов,
# у который это поле не заполнено. Используйте для этого faker.
@app.route('/fill_companies')
def fill_companies():
    """Change the empty values of the company in the table Customers"""
    new_company = fake.company()
    query = f'UPDATE customers set Company = "{new_company}" where Company is NULL '
    record = execute_query(query)

    result = format_records(record)
    return result


# Пытался поеять как сделать для каждого id разные компании так и не получилось.

app.run(port=5000, debug=True)
